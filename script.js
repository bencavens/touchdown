;(function(window,$){

    var $map = $('#map-canvas').touchdown(Touchdown.options);

    // Example - add marker
    $map.touchdown('addMarker',{
        lat: 51.132439,
        lng: 4.758665,
        content: 'food is good',
        id: 1
    });

    $map.touchdown('addMarker',{
        lat: 51.134553,
        lng: 4.748335,
        content: 'food is bad',
        id:2
    });

    $map.touchdown('setPano','#pip-pano',{
        position: {
            lat: 51.134553,
            lng: 4.748335,
        },
        pov:{
            heading:200,
            pitch:0
        },
        events:[{
            name: 'position_changed',
            callback: function(e){
                //lert('changed');
            }
        }]
    });

    $map.touchdown('addMarker',{
        location: 'Atomium, Brussels, Belgium'
    });

})(window,jQuery);

