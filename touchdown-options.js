;(function(window, google, touchdown){

    touchdown.options = {
        center: {lat: 50.850340, lng: 4.351710}, // Brussels
        //center: {lat: 40.712784, lng: -74.005941}, // new york
        centerAtCurrentLocation: false,
        zoom: 12,
        disableDefaultUI: false,
        scrollwheel: true,
        draggable: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        maxZoom: 20,
        minZoom:1,
        zoomControlOptions:{
            position: google.maps.ControlPosition.TOP_RIGHT,
            style: google.maps.ZoomControlStyle.DEFAULT
        },
        panControlOptions: {
            position:google.maps.ControlPosition.BOTTOM_LEFT
        },
        cluster: false,
        geocoder: true
    };


})(window,google, window.Touchdown);