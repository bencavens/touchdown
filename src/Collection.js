;(function(window){

    var Collection = (function(){

        function Collection(){
            this.items = [];
        }

        Collection.prototype.add = function(item){
            this.items.push(item);
        };

        Collection.prototype.remove = function(item){

            var indexOf = this.items.indexOf(item);

            if(indexOf !== -1)
            {
                this.items.splice(indexOf,1);
            }

        };

        Collection.prototype.find = function(callback,action){

            var collbackReturn,
                items = this.items,
                length = items.length,
                matches = [],
                i = 0;

            for(;i<length;i++)
            {
                callbackReturn = callback(items[i],i);

                if(callbackReturn) matches.push(items[i]);
            }

            if(action){
                action.call(this,matches);
            }

            return matches;

        };

        return Collection;

    })();

    Collection.create = function(params){
        return new Collection(params);
    }

    window.Collection = Collection;

})(window);