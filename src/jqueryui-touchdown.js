;(function(window, Touchdown){

    $.widget("touchdown.touchdown",{

        options: {},

        _create: function(){

            var element = this.element[0],
                options = this.options;

            this.map = Touchdown.create(element,options);

        },

        addMarker: function(opts){

            var self = this;

            // If location is present in the options object
            // We fetch the lat and lng via the geocode
            // Location is expected to be a valid address string
            if(opts.location){
                this.map.geocode({
                    address: opts.location,
                    success: function(results){
                        results.forEach(function(result){
                            opts.lat = result.geometry.location.lat();
                            opts.lng = result.geometry.location.lng();
                            self.map.addMarker(opts);
                        });
                    },
                    error: function(status){
                        console.error(status);
                    }
                });
            } else{
                return this.map.addMarker(opts);
            }
        },

        markers: function(){
            return this.map.markers.items;
        },

        findMarkers: function(callback){
            return this.map.findBy(callback);
        },

        removeMarkers: function(callback){
            this.map.removeBy(callback);
        },

        setPano: function(selector,opts){
            var self = this, elements = $(selector);

            $.each(elements, function(key,element){
                self.map.setPano(element,opts);
            });

        },

    });

})(window, Touchdown);