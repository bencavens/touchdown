;(function(window, google, Collection){

    var Touchdown = (function(){

        function Touchdown(element, opts){

            this.gMap = new google.maps.Map(element,opts);
            this.markers = Collection.create();

            if(opts.cluster)
            {
                this.markerClusterer = new MarkerClusterer(this.gMap,[],opts.cluster.options);
            }

            if(opts.geocoder)
            {
                this.geocoder = new google.maps.Geocoder();
            }

            if(opts.centerAtCurrentLocation)
            {
                if(navigator.geolocation)
                {
                    navigator.geolocation.getCurrentPosition(function(position){
                        self.gMap.setCenter({
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        });
                    });
                }
            }

        }

        // Prototypes
        Touchdown.prototype = {
            zoom: function(level){
                if(level){
                    this.gMap.setZoom(level);
                }else{
                    return this.gMap.getZoom();
                }
            },
            listen: function(opts){

                var self = this;

                google.maps.event.addListener(opts.obj,opts.event,function(e){
                    opts.callback.call(self,e,opts.obj);
                });
            },
            geocode: function (opts)
            {
                this.geocoder.geocode({
                    address:opts.address
                },function(results,status){
                    if(status === google.maps.GeocoderStatus.OK)
                    {
                        opts.success.call(this,results,status);
                    }
                    else{
                        opts.error.call(this,status);
                    }
                });
            },
            setPano: function(element,opts){
                var panorama = new google.maps.StreetViewPanorama(element,opts);

                if(opts.events){
                    this._attachEvents(panorama,opts.events);
                }

                this.gMap.setStreetView(panorama);

            },
            addMarker: function(opts)
            {
                var marker, self = this;

                opts.position = {
                    lat: opts.lat,
                    lng: opts.lng
                };

                marker = this._createMarker(opts);

                if(this.markerClusterer)
                {
                    // Suck into the clusterer
                    this.markerClusterer.addMarker(marker);
                }

                this._pushMarker(marker);

                if(opts.events){
                    this._attachEvents(marker,opts.events);
                }

                if(opts.content){
                    this.listen({
                        obj:marker,
                        event:'click',
                        callback: function(e){
                            var infoWindow = new google.maps.InfoWindow({
                                content: opts.content
                            });

                            infoWindow.open(this.gMap,marker);
                        }
                    });
                }
                return marker;
            },
            _attachEvents: function(obj, events){

                var self = this;

                events.forEach(function(event){
                    self.listen({
                        obj: obj,
                        event: event.name,
                        callback: event.callback
                    });
                });
            },
            _pushMarker: function(marker){
                this.markers.add(marker);
            },
            removeBy: function(callback){

                var self = this;

                self.markers.find(callback,function(markers){
                    markers.forEach(function(marker){

                        if(self.markerClusterer)
                        {
                            self.markerClusterer.removeMarker(marker);
                        }

                        else{
                            marker.setMap(null);
                        }
                    });
                });
            },
            findBy: function(callback){
                return this.markers.find(callback);
            },
            findMarkerByLat: function(lat){

                return this.markers.find(function(marker){
                    return marker.position.lat() == lat;
                });

            },
            _createMarker: function(opts){

                opts.map = this.gMap;

                return new google.maps.Marker(opts);
            }
        };

        return Touchdown;

    })();

    Touchdown.create = function(element,options){
        return new Touchdown(element,options);
    }

    window.Touchdown = Touchdown;

})(window, google, Collection)